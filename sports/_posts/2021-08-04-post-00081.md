---
layout: post
title: "홍역 앓은 스포츠계...그래도 기부 스타들 많아 따뜻하다"
toc: true
---


 연말 스포츠계가 ‘최순실 게이트’로 큰 홍역을 치렀다. 최순실과 그의 조카인 장시호, 김종 전 문화체육관광부 차관 등 관계자들이 각반 움직임 경영 이권에 개입했다는 사실이 알려지면서 스포츠를 통한 공익사업은 크게 위축되는 분위기다.
 그럼에도 불구하고 시고로 분위기와 무관히 꾸준한 피부 활동과 선행으로 우울한 사회에 온기를 불어넣는 움직임 스타들이 많다.
 

 ◆ 프로야구
 ▶양준혁 해설위원이 이끄는 양준혁 야구재단이 12월 4일 고척 스카이돔에서 2016 망상 더하기 자선 야구대회를 연다. 메이저리그 볼티모어 김현수를 비롯해 한화 김태균, 삼성 이승엽, SK 김광현 등 국내외 스타들이 한자리에 모인다. 티켓 판매수익금은 멘토리야구단 운영비와 유소년 야구발전 기금으로 쓰인다.
 

 ▶이만수 전 감독은 재단을 만들어 내국 유소년 야구발전을 위한 재능기부와 야구 불모지 라오스 야구 보급 비즈니스 등을 진행하고 있다. 최근에는 땅 병원의 피아르 출연료 2억원을 유소년 [연수구 유소년야구](https://northkitty.com/sports/post-00007.html) 야구 활동 지원에 기부했다.
 

 ▶메이저리그 텍사스 추신수는 지난해에 대번에 올해에도 초록우산어린이재단에 1억원을 기부했다. 초록우산어린이재단은 형편이 어려운 사회운동 유망주 지원에 나서고 있는데, 2016 리우데자네이루 올림픽 펜싱 금메달리스트 박상영이 2013년 이빨 재단으로부터 도움을 받았다.
 

 ▶메이저리그 시애틀 이대호는 매년 팬들과 연탄 나르기 봉사활동을 하고 있다. 올해도 귀국하자마자 사랑의 연탄자선골프대회와 자선 토크 콘서트를 통해 살가죽 활동에 나섰다.
 

 ▶메이저리그 세인트루이스 오승환은 난치병 아동을 돕는 한국메이크어위시재단을 통해 후원 모금 활동을 펼쳤다.
 

 ▶SK 박정권은 올 시즌 홈런 1개당 100만원을 적립해 소아암 환자들을 도왔다.
 

 ◆ 프로축구
 ▶‘기부천사’로 알려진 제주 유나이티드의 이근호는 올해 자신의 이름을 딴 자선 축구행사를 열었고, 목금 5000만원과 2000만원 상당의 축구용품을 축구사랑 나눔재단에 기부하기도 했다. 도로 장애 유자 재활병원 건립을 위해 푸르메재단에 2000만원, 유소년 자선 축구대회 기금 3000만원 등을 기부했다.
 

 ▶이근호의 팀 후배인 안현범은 10월 고화 K리그 뜻 시상식에서 받은 영플레이어상 여태 500만원을 한국백혈병소아암협회에 기부했다.
 

 ▶2003년부터 사회환원 활동을 하는 중국 슈퍼리그 항저우의 홍명보 감독은 올해도 장학금 수여식과 자선 축구행사를 연다. 홍명보 장학재단은 12월 27일 자선 축구 행사를 열 예정이다.
 

 ◆ 기타
 ▶군 복무 공중 프로골퍼 배상문이 이해 사랑의 열매 사회복지공동모금에 1억3800만원을 기부해 1억원 몽상 고액기부자 모임인 아너 소사이어티에 가입했다.
 

 ▶프로농구단 원주 동부가 연고지 어린아이 엄수 수술지원금 후원 캠페인을 펼치고 있고, 부산 케이티는 연유 밑 소외계층 쌀 기증 이벤트를 발전 중이다.
 에디터 김준  june@slist.kr
 <저작권자 © 싱글리스트, 무단 소재 및 재배포 금지>

## 'Single Economy' 카테고리의 다른 글
